const config = require('../config')
const WSServer = require('./WebSocketServer')
ws_server = new WSServer(config)
ws_server.start().then(()=>{
    console.log("ws connection established from client and server")
})
