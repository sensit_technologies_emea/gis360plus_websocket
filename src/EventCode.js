module.exports = {
    GPS_INVALID: -2,
    GPS_HIDDEN: -1,
    GPS_VSR_POINT: 0,
    GPS_POINT: 1,
    MISSION_START: 11,
    LEAK: 21,
    LEAK_IMPORTANT: 22,
    LEAK_TOLOCATE: 23,
    LEAK_REJECTED: 24,
    SIGNAL: 24,
    ANNOTATION: 31,
    MISSION_PAUSE: 41,
    MISSION_RESTART: 49,
    PAUSE: 51,
    RESTART: 59,
    TEST_START: 61,
    TEST_END: 62,
    ERROR_PUMP: 70,
    SPEED_EXCEEDED: 71,
    ERROR_SYS: 72,
    ERROR_GPS: 73,
    ERROR_END: 79,
    MISSION_END: 99
}