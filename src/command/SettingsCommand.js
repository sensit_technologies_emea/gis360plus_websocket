const ModbusCommand = require('./ModbusCommand')

class SettingsCommand extends ModbusCommand{

    constructor(opts) {
        super(opts)
        let firstByte = "0x"+this.machine_id.toString(16)
        this.setBytes( [firstByte, 0x04, 0xb3, 0xb0, 0x00, 0x14, 0x00, 0x00] )
    }

}

module.exports = SettingsCommand