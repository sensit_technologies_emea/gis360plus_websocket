const BaseCommand = require('./BaseCommand')
const RealTimeResponse = require('../response/RealTimeResponse')

class ModbusCommand extends BaseCommand{

    constructor(opts) {
        super(opts)
        this.machine_id = this.options.private._server.machine_id
        if(this.machine_id==null) this.machine_id = 49
        this.bytes = []
    }

    setBytes(bytes){
        this.bytes = bytes
    }

    message(){
        RealTimeResponse.updateCRC(this.bytes)
        return Buffer.from(this.bytes)
    }

    run() {
        let ws = this.options.private._ws
        ws.tcpip_client.send(this.message())
    }

}

module.exports = ModbusCommand