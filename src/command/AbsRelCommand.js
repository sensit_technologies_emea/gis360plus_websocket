const ModbusCommand = require('./ModbusCommand')

class AbsRelCommand extends ModbusCommand{

    constructor(opts) {
        super(opts)
        let firstByte = "0x"+this.machine_id.toString(16)
        let value = this.options.message.status ? 0xff : 0x00
        //(33)(5)(0)(a)(ff)(0)(a8)(2a)
        this.setBytes( [firstByte, 0x05, 0x27, 0x1a, value, 0x00, 0x00, 0x00] )
    }

}

module.exports = AbsRelCommand