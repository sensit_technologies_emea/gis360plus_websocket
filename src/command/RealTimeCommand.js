const BaseCommand = require('./BaseCommand')
const RealTimeResponse = require('../response/RealTimeResponse')

//const READ_COMMAND = Buffer.from([0x31, 0x04, 0xa7, 0xf8, 0x00, 0x1e, 0xd7, 0x77])

class RealTimeCommand extends BaseCommand{
    constructor(opts) {
        super(opts)
        let machine_id = this.options.private._server.machine_id
        if(machine_id==null) machine_id = 49
        let firstByte = "0x"+machine_id.toString(16)
        //let arrayBytes = [firstByte, 0x04, 0xa7, 0xf8, 0x00, 0x1e, 0xd7, 0x77]
        let arrayBytes = [firstByte, 0x04, 0xa7, 0xf8, 0x00, 0x26, 0x00, 0x00]
        RealTimeResponse.updateCRC(arrayBytes)
        //console.log(arrayBytes)
        this.read_command = Buffer.from(arrayBytes)
    }

    run() {
        let ws = this.options.private._ws
        ws.tcpip_client.send(this.message())
    }

    message(){
        return this.read_command
    }

}

module.exports = RealTimeCommand

