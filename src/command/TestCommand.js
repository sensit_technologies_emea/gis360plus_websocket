const ModbusCommand = require('./ModbusCommand')

class TestCommand extends ModbusCommand{

    constructor(opts) {
        super(opts)
        let firstByte = "0x"+this.machine_id.toString(16)
        let value = this.options.message.type=="I" ? 0x15 : 0x16
        this.setBytes( [firstByte, 0x05, 0x27, value, 0xff, 0x00, 0x00, 0x00] )
    }

}

module.exports = TestCommand