const StartCommand = require('./StartCommand')
const ConnectCommand = require('./ConnectCommand')
const RealTimeCommand = require('./RealTimeCommand')
const PumpCommand = require('./PumpCommand')
const CleanLineCommand = require('./CleanLineCommand')
const TestCommand = require('./TestCommand')
const UnknownCommand = require('./UnknownCommand')
const SettingsCommand = require('./SettingsCommand')
const AbsRelCommand = require('./AbsRelCommand')
const AlarmCommand = require('./AlarmCommand')
const TestGetResultCommand = require('./TestGetResultCommand')


class CommandFactory {

    static create(options) {
        let command = null
        options.type = options.type || "unknown"
        switch(options.type){
            case "realtime":
                console.log("creating command <RealTimeCommand>")
                command = new RealTimeCommand(options)
                break
            case "connect":
                console.log("creating command <ConnectCommand>")
                command = new ConnectCommand(options)
                break
            case "start":
                console.log("creating command <StartCommand>")
                command = new StartCommand(options)
                break
            case "cleanline":
                console.log("creating command <CleanLineCommand>")
                command = new CleanLineCommand(options)
                break
            case "pump":
                console.log("creating command <PumpCommand>")
                command = new PumpCommand(options)
                break
            case "absrel":
                console.log("creating command <AbsRelCommand>")
                command = new AbsRelCommand(options)
                break
            case "alarm":
                console.log("creating command <AlarmCommand>")
                command = new AlarmCommand(options)
                break
            case "test":
                console.log("creating command <TestCommand>")
                command = new TestCommand(options)
                break
            case "testgetresult":
                console.log("creating command <TestGetResultCommand>")
                command = new TestGetResultCommand(options)
                break
            case "settings":
                console.log("creating command <SettingsCommand>")
                command = new SettingsCommand(options)
                break
            default:
                console.log("creating command <UnknownCommand>")
                command = new UnknownCommand(options)
                break
        }
        return command
    }

}

module.exports = CommandFactory