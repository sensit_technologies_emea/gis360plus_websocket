const BaseCommand = require('./BaseCommand')

class UnknownCommand extends BaseCommand{
    constructor(options) {
        super(options)
    }
    run(){
        let ws = this.options.private._ws
        ws.tcpip_client.send(this.message())
    }

}

module.exports = UnknownCommand