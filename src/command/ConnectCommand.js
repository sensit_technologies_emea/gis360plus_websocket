const BaseCommand = require('./BaseCommand')


class ConnectCommand extends BaseCommand{

    constructor(opts) {
        super(opts)
    }

    run() {
        let host = this.options.message.host
        let port = this.options.message.port
        let ws = this.options.private._ws
        let _server = this.options.private._server
        ws.tcpip_client.connect(host, port, {
            onConnect: function(){
                //console.log("onConnect...")
                _server.onServerConnect(ws)
            },
            onData: function(data){
                //console.log("onData...")
                // Controllo ritardo comunicazione
                let minDuration = _server.minTimePerRequest()
                let duration = new Date().getTime() - ws.lastRequestDate
                let delay = minDuration - duration
                if(delay < 1) delay = 1
                _server.sleep(delay).then(() => {
                    _server.onServerData(ws, data)
                })
            },
            onClose: function(){
                //console.log("onClose...")
                _server.onServerClose(ws)
            }
        })

    }

}

module.exports = ConnectCommand