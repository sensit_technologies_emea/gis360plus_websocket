const BaseCommand = require('./BaseCommand')

class StartCommand extends BaseCommand{

    constructor(opts) {
        super(opts)
    }

    message(){
        return null
    }
}

module.exports = StartCommand