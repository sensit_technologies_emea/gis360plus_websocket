
class BaseCommand {
    /**
     * Constrcutor of the class
     * @param opts
     */
    constructor(options) {
        this.options = options
        this.options.type = options.type || 'unknown'
        this.options.message = options.message || null
        if(!options)
            throw new Error("Missing options object")
    }

    message(){
        return this.options.message
    }

    run(){
        console.log("run: " + this.message())
    }
}

module.exports = BaseCommand