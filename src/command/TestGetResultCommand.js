const ModbusCommand = require('./ModbusCommand')

class TestGetResultCommand extends ModbusCommand{

    constructor(opts) {
        super(opts)
        let firstByte = "0x"+this.machine_id.toString(16)
        let value = this.options.message.type=="I" ? 0xe0 : 0xee
        this.setBytes( [firstByte, 0x03, 0x79, value, 0x00, 0x1e, 0x00, 0x00] )
    }

}

module.exports = TestGetResultCommand