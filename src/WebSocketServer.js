const WebSocket = require('ws')
const terminator = require('http-terminator')
const TcpIpClient = require('./TcpIpClient')
const ResponseFactory = require('./response/ResponseFactory')
const UnknownResponse = require('./response/UnknownResponse')
const RealTimeResponse = require('./response/RealTimeResponse')
const SettingsResponse = require('./response/SettingsResponse')
const TestGetResultResponse = require('./response/TestGetResultResponse')
const CommandFactory = require('./command/CommandFactory')
const FilterChain = require('./filter/FilterChain')
const SyncQueue = require('./sync/Queue')
const Package = require('./sync/Package')
const Utils = require('./Utils')
const EventCode = require('./EventCode')

let MIN_TIME_REQUEST=null

class WebSocketServer {

    /**
     * Constructor
     * @param protocol - web socket protocol ['ws', 'wss']
     * @param port - listening port number
     */
    constructor(config, type){
        config.filters = config.filters || {}
        config.synchronizer = config.synchronizer || {}
        //console.log("Creating websocket ["+ protocol + "] on port " + port + "...")
        this.server = null
        this.websocket = null
        this.machine_id = null
        this.config = config
        this._on_connect_ok = null
        this._on_connect_no = null
        this.type = type || 'realtime'
        this.filter_chain = new FilterChain(config.filters)
        this.sync_queue = new SyncQueue(config.synchronizer)
        this.package = new Package({items:[], max_size: config.synchronizer.options.size})
        this.last_gps_timestamp = 0
        this.session_id = null
        this.measure = null
        this.event_code = EventCode.GPS_POINT
        this.request_timeout = config.tcpip.timeout || 200
    }

    minTimePerRequest(){
        if(null == MIN_TIME_REQUEST){
            let maxRate = 0
            if(this.config.maxRate)
                maxRate = this.config.maxRate
            if(maxRate <= 0)
                maxRate = 5
            MIN_TIME_REQUEST = 1000/maxRate
        }
        return MIN_TIME_REQUEST
    }

    /**
     * Create a 32 characters UUID
     * @returns {string}
     */
    getUniqueID () {
        return Utils.getUniqueID()
    }

    /**
     * Create and start a WebSocket Server
     * @returns {Promise<unknown>}
     */
    start() {
        var _this = this
        console.log(`WebSocketServer.start: protocol => ${this.config.websocket.protocol}, port => ${this.config.websocket.port}`);
        if (this.config.websocket.protocol == "ws") {
            const http = require('http')
            this.server = http.createServer()
        } else {
            const https = require('https')
            const fs = require('fs')
            this.server = https.createServer({
                cert: fs.readFileSync(this.config.websocket.cert),
                key: fs.readFileSync(this.config.websocket.key)
            })
        }
        return new Promise((resolve, reject) => {
            _this._on_connect_ok = resolve
            _this._on_connect_no = reject
            _this.server.listen(_this.config.websocket.port)
            _this.websocket = new WebSocket.Server({server: _this.server})
            _this.websocket.on('connection', _this.onClientConnection.bind(_this))
        })
    }

    /**
     * Stop the WebSocket server
     */
    stop(){
        if(this.server==null) return
        const httpTerminator = terminator.createHttpTerminator({server: this.server})
        return httpTerminator.terminate()
    }

    // sleep time expects milliseconds
    sleep (time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    /**
     * Handle a ws client connection
     * @param ws
     */
    onClientConnection(ws){
        let _server = this
        ws.id = this.getUniqueID()
        ws.lastRequestDate = new Date().getTime()
        ws.inProgress = false
        console.log(`WebSocketServer.onClientConnection: Peer ${ws.id} connected.`);
        ws.tcpip_client = new TcpIpClient(ws.id, ws, this.request_timeout)

        ws.on('message', function(message){
            //console.log("ws.on('message')...")
            _server.onClientMessage(ws, message)
        })

        ws.on('close', function(reasonCode, description) {
            //console.log("ws.on('close')...")
            _server.onClientClose(ws, reasonCode, description)
        })

        //ws.send(`HELLO ${ws.id}`)
        if(this._on_connect_ok)
            this._on_connect_ok()
    }

    /**
     * Handle a ws client message event and redirect it to the TCP/IP server
     * @param ws - websocket client object
     * @param message - message received from the client
     */
    onClientMessage(ws, message){
        //console.log(`WebSocketServer.onClientMessage: message => ${message} from Peer ${ws.id}`)
        console.log("RECEIVING REQUEST => ws.id: " + ws.id)
        let _this = this;
        if(!ws.inProgress){
            /*
            ws.timeout_id = setTimeout(function(){
                let resp = {
                    type: "timeout",
                    valid: false,
                    message: "request is in timeout"
                }
                console.error("Timeout response!!!")
                console.log(ws.status)
                ws.tcpip_client.send(JSON.stringify(resp))
                //_this.onClientConnection(ws)
            }, this.request_timeout)
            */

            let jsonMessage = null
            try{
                jsonMessage = JSON.parse(message)
            }catch(e){
                console.warn(e.message)
                jsonMessage = { type: message, message }
            }
            if(jsonMessage.hasOwnProperty("message")){
                if(jsonMessage.message.hasOwnProperty("machine"))
                    this.machine_id = jsonMessage.message.machine
            }
            if(jsonMessage.hasOwnProperty("sessionId")){
                this.session_id = jsonMessage.sessionId;
            }
            if(jsonMessage.hasOwnProperty("eventcode"))
                this.event_code = jsonMessage.eventcode;
            if(jsonMessage.hasOwnProperty("measure"))
                this.measure = jsonMessage.measure;
            jsonMessage.private = { _ws: ws, _server: this }
            ws.lastRequestDate = new Date().getTime()
            let command = CommandFactory.create(jsonMessage)
            command.run()
        }else{
            let resp = {
                type: "busy",
                valid: false,
                message: "another request is in progress"
            }
            ws.tcpip_client.send(JSON.stringify(resp))
        }
    }

    /**
     * Handle a ws client connection close event
     * @param ws - websocket client object
     * @param reasonCode - code's reason of the disconnection
     * @param description - description of the disconnection
     */
    onClientClose(ws, reasonCode, description){
        //console.log(`WebSocketServer.onClientClose: Peer ${ws.id} disconnected.`);
        ws.tcpip_client.disconnect()
    }

    onServerConnect(ws){
        //console.log("Connected to the TCP/IP Server")
        ws.send("HELLO " + ws.id)
    }

    onServerData(ws, data){
        try{
            if(data == null){
                console.log("SENDING NULL => ws.id: " + ws.id)
                ws.send(JSON.stringify({type: "null"}))
                return
            }
            //if(ws.timeout_id)
            //    clearTimeout(ws.timeout_id)
            ws.inProgress = false
            console.log("SENDING RESPONSE => ws.id: " + ws.id)
            let response = ResponseFactory.create({type: this.type, message: data})
            let json_response = response.parse()
            if(response instanceof UnknownResponse){
                console.log("response to the client => 1")
                ws.send(json_response)
            }else{
                //let new_package = false
                if(json_response.type == "realtime")
                    this.filter_chain.check(json_response)

                if(json_response.type == "testgetresult")
                    console.log("++++++++++++++++++++")

                if(response instanceof RealTimeResponse){
                    if(response instanceof SettingsResponse){
                        console.log("++++++++++ SETTINGS ++++++++++")
                    }else if(response instanceof TestGetResultResponse){
                        console.log("++++++++++ TESTGETRESULT ++++++++++")
                    } else {
                        if(this.event_code != null)
                            json_response.event_code = this.event_code
                        if(this.measure != null)
                            json_response.data.sys.concentration = this.measure
                        if(json_response.event_code == EventCode.GPS_POINT && this.last_gps_timestamp == json_response.data.gps.timestamp)
                            json_response.event_code = EventCode.GPS_HIDDEN
                        else{
                            try{
                                if(json_response.valid)
                                    this.last_gps_timestamp = json_response.data.gps.timestamp
                            }catch(e){
                                console.error(e);
                            }
                        }
                        json_response.sessionId = this.session_id;
                        if(this.config.synchronizer.enabled){
                            if(!this.package.hasSpace()){
                                //new_package = true
                                this.sync_queue.put(this.package.items)
                                this.package = new Package({
                                    items:[],
                                    max_size: this.config.synchronizer.options.size
                                })
                            }
                            if(json_response.valid)
                                this.package.add(json_response)
                            console.log("add to package, with size: " + this.package.size())

                            if(this.event_code == EventCode.MISSION_END){
                                console.log("Mission END")
                                this.sync_queue.put(this.package.items)
                                this.package = new Package({
                                    items:[],
                                    max_size: this.config.synchronizer.options.size
                                })
                            }

                        }
                        this.event_code = null
                        this.measure = null
                    }
                }
                console.log("response to the client => 2")
                ws.send(JSON.stringify(json_response))
                /*
                if(!new_package){
                    //console.log(JSON.stringify(json_response))
                    ws.send(JSON.stringify(json_response))
                }else{
                    this.sleep(1000).then(()=>{
                        ws.send(JSON.stringify(json_response))
                    })
                }
                */
            }
        }catch(ex){
            console.error(ex)
            ws.send(JSON.stringify({valid: false}))
        }
    }
    onServerClose(ws){
        //console.log("TCP/IP Server closed")
        ws.send("TCP/IP Server closed")
    }

}

module.exports = WebSocketServer