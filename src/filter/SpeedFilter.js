const BaseFilter = require('./BaseFilter')
const EventCode = require('../EventCode')

class SpeedFilter extends BaseFilter{

    /**
     * Check if the data is under the speed limit
     * @param json data to check
     * @returns {boolean}
     */
    check(json){
        if(json.valid && json.data.gps.speed > this.options.limit){
            json.event_code = EventCode.SPEED_EXCEEDED
            return false
        }
        return true
    }

}

module.exports = SpeedFilter