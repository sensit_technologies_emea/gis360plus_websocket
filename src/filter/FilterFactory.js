const SpeedFilter = require('./SpeedFilter')
const ConcentrationFilter = require('./ConcentrationFilter')

class FilterFactory {

    static create(options) {
        let filter = null
        let type = options.type || "empty"
        let opts = options.options || {}
        switch(type){
            case "speed":
                filter = new SpeedFilter(opts)
                break
            case "concentration":
                filter = new ConcentrationFilter(opts)
                break
            default:
                filter = new EmptyFilter(opts)
                break
        }
        return filter
    }

}

module.exports = FilterFactory