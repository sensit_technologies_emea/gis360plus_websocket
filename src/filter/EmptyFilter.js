const BaseFilter = require('./BaseFilter')

class EmptyFilter extends BaseFilter{

    /**
     * Check if the data is under the speed limit
     * @param data
     * @returns {boolean}
     */
    check(data){
        return true
    }

}

module.exports = EmptyFilter