const BaseFilter = require('./BaseFilter')
const EventCode = require('../EventCode')

class ConcentrationFilter extends BaseFilter{

    constructor(options){
        super(options)
        this.active=false
    }

    /**
     * Check if the data is under the saturation limit
     * @param json data to verify
     * @returns {boolean}
     */
    check(json){
        if(json.valid && !this.active){
            if(json.data.las.concentration > this.options.limit){
                this.active = true
                json.event_code = EventCode.LEAK
                return false
            }
        }
        if(json.valid)
            this.active = (json.data.las.concentration > this.options.limit)
        return true
    }

}

module.exports = ConcentrationFilter