const FilterFactory = require('./FilterFactory')

class FilterChain {

    constructor(filters){
        filters = filters || []
        if(!Array.isArray(filters))
            filters = [filters]
        this.filters = []
        for(let i=0; i<filters.length; i++){
            let filter = FilterFactory.create({
                type: filters[i].name,
                options: filters[i].options
            })
            this.add(filter)
        }
    }

    /**
     * Add a filter to the chain
     * Filter added before has higher priority than filter added after
     * If a filter check is false the chain is stopped
     * @param filter
     */
    add(filter){
        this.filters.push(filter)
    }

    /**
     * Check data using the filters chain
     * @param data
     * @returns {boolean}
     */
    check(data){
        let result = true
        for(let i=0; i<this.filters.length; i++){
            if(!this.filters[i].check(data)){
                result=false
                break
            }
        }
        return result
    }

}

module.exports = FilterChain