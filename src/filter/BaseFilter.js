class BaseFilter {
    constructor(options){
        this.options = options
    }
    check(data){
        throw new Error("Method not implemented")
    }
}

module.exports = BaseFilter