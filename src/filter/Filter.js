BaseFilter = require('./BaseFilter')
EmptyFilter = require('./EmptyFilter')
SpeedFilter = require('./SpeedFilter')
ConcentrationFilter = require('./ConcentrationFilter')
FilterFactory = require('./FilterFactory')
FilterChain = require('./FilterChain')

module.exports = {
    BaseFilter,
    EmptyFilter,
    SpeedFilter,
    ConcentrationFilter,
    FilterFactory,
    FilterChain
}