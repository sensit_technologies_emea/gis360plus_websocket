const BaseResponse = require('./BaseResponse')

class UnknownResponse extends BaseResponse{
    constructor(options) {
        super(options)
    }
    parse() {
        let message = this.options.message
        //for(var i=0; i<message.length; i++)
        //    console.log(message[i])
        if(message instanceof Buffer)
            message = message.toString('utf-8')
        console.error("Unknow response: " + message)
        return message
    }
}

module.exports = UnknownResponse