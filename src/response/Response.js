BaseResponse = require('./BaseResponse')
RealTimeResponse = require('./RealTimeResponse')
SettingsResponse = require('./SettingsResponse')
TestGetResultResponse = require('./TestGetResultResponse')
UnknownResponse = require('./UnknownResponse')
ResponseFactory = require('./ResponseFactory')

module.exports = {
    BaseResponse,
    RealTimeResponse,
    SettingsResponse,
    TestGetResultResponse,
    UnknownResponse,
    ResponseFactory
}