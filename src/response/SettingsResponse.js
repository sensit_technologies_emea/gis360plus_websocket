const number_util = require('number-util')
const RealTimeResponse = require('./RealTimeResponse')

class SettingsResponse extends RealTimeResponse{

    constructor(opts) {
        super(opts)
        this.options.type = "settings"
    }

    /**
     //	float 			SET_Test_Bottle_Value;              	// WORD 1  LOW WORD , 2 HIGH WORD
     //	float 			SET_External_test_THRESHOLD;            // WORD 3  LOW WORD , 4 HIGH WORD
     //	float 			SET_External_test_TOLERANCE;            // WORD 5  LOW WORD , 6 HIGH WORD
     //	float 			SET_Internal_test_THRESHOLD;            // WORD 7  LOW WORD , 8 HIGH WORD
     //	float 			SET_Internal_test_TOLERANCE;            // WORD 9 LOW WORD , 10 HIGH WORD
     //	float 			SET_speed_THRESHOLD;          			// WORD 11  - LOW WORD , 12 HIGH WORD
     //	float 			SET_speed_TOLERANCE;          			// WORD 13  - LOW WORD , 14 HIGH WORD
     //	float 			SET_GasConcentration_THRESHOLD_1;       // WORD 15 LOW WORD , 16 WORD HIGH WORD
     //	float 			SET_GasConcentration_THRESHOLD_2;       // WORD 17 LOW WORD , 18 WORD HIGH WORD
     * @returns {{valid: boolean, data: {}}}
     */
    parse() {
        //[ID][FC][BC][DATA(1+)][CRC]
        let message = this.options.message || null
        let json = {
            valid: false,
            data: {}
        }
        if(message.length >= 5){
            let functionId = message.readUIntBE(1, 1);  // Legge il byte N.2
            let dataLength = message.readUIntBE(2, 1);  // Legge il byte N.3
            if (functionId == 4 && (message.length === 3 + dataLength + 2) && this.checkCRC(message)){
                json.type = "settings"
                json.valid = true
                json.data.test = {
                    bottle: this.readFloat(0),
                    ext_threshold: this.readFloat(4),
                    ext_tolerance: this.readFloat(8),
                    int_threshold: this.readFloat(12),
                    int_tolerance: this.readFloat(16)
                }
                json.data.speed = {
                    threshold: this.readFloat(20),
                    tolerance: this.readFloat(24)
                }
                json.data.gas = {
                    conc_threshold_1: this.readFloat(28),
                    conc_threshold_2: this.readFloat(32)
                }
                json.data.sys = {
                    serial: this.readUInt32(36)
                }
            }
        }
        return json
    }
}

module.exports = SettingsResponse