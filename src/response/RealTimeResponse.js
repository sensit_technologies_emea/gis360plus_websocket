const number_util = require('number-util')
const BaseResponse = require('./BaseResponse')
const EventCode = require('../EventCode')
const StatusCode = require('../StatusCode')
const { crc16modbus } = require('crc');

let _COUNTER = 0
let _LAST_STATUS = 30

class RealTimeResponse extends BaseResponse{

    constructor(opts) {
        super(opts)
        this.options.base_time = 0
        this.shift = 3
    }

    static getCRC(message){
        let crc = ("0000" + crc16modbus(message.slice(0, message.length-2)).toString(16)).slice(-4)
        return crc.substring(2,4) + crc.substring(0,2)
    }

    static updateCRC(message){
        let CRC = RealTimeResponse.getCRC(message)
        let CRC1 = "0x"+CRC.substring(0,2)
        let CRC2 = "0x"+CRC.substring(2,4)
        message[6]=CRC1
        message[7]=CRC2
    }

    checkCRC(message){
        let msgCRC1 = message[message.length-2].toString(16)
        let msgCRC2 = message[message.length-1].toString(16)
        if(msgCRC1.length==1) msgCRC1 = "0" + msgCRC1
        if(msgCRC2.length==1) msgCRC2 = "0" + msgCRC2
        let msgCRC = msgCRC1 + msgCRC2
        let chkCRC = RealTimeResponse.getCRC(message)
        if(chkCRC===msgCRC)
            return true
        //if(chkCRC.length==2) chkCRC += "00"
        console.error("Invalid CRC, " + msgCRC + " => " + chkCRC)
        return false
    }

    bytesToFloat32(data, startIndex) {
        let value = ((data[startIndex+0] & 0xff) << 8) |
            (data[startIndex+1] & 0xff) |
            ((data[startIndex+2] & 0xff) << 24) |
            ((data[startIndex+3] & 0xff) << 16);
        let float_value = number_util.intBitsToFloat(value)
        return float_value
    }

    bytesToDouble(data, startIndex) {
        let array = [
            data[startIndex+6],
            data[startIndex+7],
            data[startIndex+4],
            data[startIndex+5],
            data[startIndex+2],
            data[startIndex+3],
            data[startIndex+0],
            data[startIndex+1]
        ]
        let buf = Buffer.from(array)
        return buf.readDoubleBE(0)
    }

    getLonLat(){
        let res=[null, null, false]
        let lat_in = null
        let lon_in = null
        if(this.shift + 18 <= this.options.message.length - 2){
            //Latitude
            lat_in = this.bytesToDouble(this.options.message, this.shift+18)
            let lat_deg = Math.floor(lat_in/100.0)
            let lat_min = lat_in - lat_deg * 100.0
            let lat = lat_deg + lat_min/60.0
            res[1] = lat
        }
        if(this.shift + 26 <= this.options.message.length - 2){
            //Longitude
            lon_in = this.bytesToDouble(this.options.message, this.shift+26)
            let lon_deg = Math.floor(lon_in/100.0)
            let lon_min = lon_in - lon_deg * 100.0
            let lon = lon_deg + lon_min/60.0
            res[0] = lon
        }
        if((res[0]>=-180.0 && res[0]<=180.0) &&
            (res[1]>=-90.0 && res[1]<=90.0)
        ){
            res[2] = true
        }else{
            console.error("INCORRECT GPS COORDINATE", lon_in, lat_in)
        }

        return res
    }

    /**
     * Read 2 bytes from the "data" buffer starting from index and convert them to an UInt16 value
     * @param index
     * @returns {*}
     */
    readUInt16(index){
        if(this.shift + index + 1 > this.options.message.length - 2)
            return null
        return this.options.message.readUIntBE(this.shift + index)*256
        + this.options.message.readUIntBE(this.shift + index + 1)
    }

    /**
     * Read 4 bytes from the "data" buffer starting from index and convert them to an UInt32 value
     * @param index
     * @returns {*}
     */
    readUInt32(index){
        if(this.shift + index + 3 > this.options.message.length - 2)
            return null
        return this.options.message.readUIntBE(this.shift + index + 2)*256*256*256
        + this.options.message.readUIntBE(this.shift + index + 3)*256*256
        + this.options.message.readUIntBE(this.shift + index + 0)*256
        + this.options.message.readUIntBE(this.shift + index + 1)
    }

    /**
     * Read 4 bytes from the "data" buffer starting from index and convert them to an Float value
     * @param index
     * @returns {*}
     */
    readFloat(index){
        if(this.shift + index + 3 > this.options.message.length - 2)
            return null
        let value = ((this.options.message[this.shift+index+0] & 0xff) << 8) |
            (this.options.message[this.shift+index+1] & 0xff) |
            ((this.options.message[this.shift+index+2] & 0xff) << 24) |
            ((this.options.message[this.shift+index+3] & 0xff) << 16);
        let float_value = number_util.intBitsToFloat(value)
        return float_value
    }

    isError(status){
        return status >= 400;
    }

    status2event(status){
        let result = null
        switch(status){
            case StatusCode.START_IT:
            case StatusCode.START_ET:
                result=EventCode.TEST_START
                break
            case StatusCode.CHECK_VALUE_IT:
            case StatusCode.CHECK_VALUE_ET:
                result=EventCode.TEST_END
                break
            case StatusCode.ERROR_PUMP:
                result=EventCode.ERROR_PUMP
                break
            case StatusCode.ERROR_SPEED:
                result=EventCode.SPEED_EXCEEDED
                break
            case StatusCode.ERROR_GPS:
                result=EventCode.ERROR_GPS
                break
            case StatusCode.ERROR_LASER:
                result=EventCode.ERROR_SYS
                break
            default:
                result = EventCode.GPS_POINT
                break
        }
        return result
    }

    /**
     //	uint32_t 		SYS_Serial_Nr;    	// WORD 1 - LOW WORD , WORD 2 HIGH WORD
     //	uint32_t 		SYS_TimeStamp;   	// WORD 3 - LOW WORD , WORD 4 HIGH WORD
     //	uint16_t    	SYS_pumpStatus;  	// WORD 5
     //	uint16_t    	SYS_batteryLevel;	// WORD 6
     //	uint16_t    	SYS_Status;  		// WORD 7
     //	uint32_t 		GPS_TimeStamp;    	// WORD 8 - LOW WORD , WORD 9 HIGH WORD
     //	double	 		GPS_Latitude;		// WORD 10 - LOW WORD , WORD 11 HIGH WORD, WORD 12 - LOW WORD , WORD 13 HIGH WORD
     //	double 			GPS_Longitude;    	// WORD 14 - LOW WORD , WORD 15 HIGH WORD, WORD 16 - LOW WORD , WORD 17 HIGH WORD
     //	float 			GPS_Speed; 		    // WORD 18 - LOW WORD , WORD 19 HIGH WORD
     //	float 			GPS_Azimuth; 		// WORD 20 - LOW WORD , WORD 21 HIGH WORD
     //	uint16_t 		GPS_gpsValid;       // WORD 22
     //	uint16_t 		GPS_NS;    			// WORD 23
     //	uint16_t    	GPS_EW;				// WORD 24 - LOW WORD
     //	float    		LAS_concentration;  // WORD 25 - LOW WORD , WORD 26 HIGH WORD
     //	uint16_t 		LAS_activeState;	// WORD 27
     //	uint16_t 		LAS_counterM; 		// WORD 28
     //	uint16_t 		LAS_errorID; 		// WORD 29

     // uint16_t        SYS_UserID;				// WORD 30
     // float			SYS_Concentration;		// WORD 31 - LOW WORD , WORD 32 HIGH WORD
     // uint16_t        SYS_IsAbsolute;			// WORD 33
     // uint16_t        SYS_pressure_Pump;      // WORD 34
     // uint16_t 		SYS_Water; 				// WORD 35
     // uint16_t 		SYS_PressureTestBottle; // WORD 36
     // uint16_t 		SYS_PressureCleanBottle;// WORD 37
     // uint16_t 		SYS_IsAlarm;            // WORD 38
     * @returns {{valid: boolean, data: {}}}
     */
    parse() {
        //[ID][FC][BC][DATA(1+)][CRC]
        let message = this.options.message || null
        let json = {
            valid: false,
            data: {}
        }
        if(message.length >= 5){
            //let machineId = message.readUIntBE(0, 1); // Legge il byte N.1
            let functionId = message.readUIntBE(1, 1);  // Legge il byte N.2
            let dataLength = message.readUIntBE(2, 1);  // Legge il byte N.3
            if (functionId == 4 && (message.length === 3 + dataLength + 2) && this.checkCRC(message)){
                let shift = 3
                let lonlat = this.getLonLat()

                let ns = this.readUInt16(44)
                let ew = this.readUInt16(46)

                if(ew != 69) lonlat[0] *= -1
                if(ns != 78) lonlat[1] *= -1

                // PANIC:: temporaneo ma le coordinate errate (negative)
                // devono essere inviate correttamente sui parametri EW, NS
                lonlat[0] = Math.abs(lonlat[0])
                lonlat[1] = Math.abs(lonlat[1])

                json.type = "realtime"
                json.valid = true
                json.event_code = EventCode.GPS_POINT
                json.data.sys = {
                    serial: this.readUInt32(0),
                    timestamp: this.readUInt32(4) + this.options.base_time,
                    pumpstatus: this.readUInt16(8),
                    batterylevel: this.readUInt16(10),
                    status: this.readUInt16(12),
                    userid: this.readUInt16(58),
                    concentration: this.readFloat(60),
                    isAbsolute: this.readUInt16(64),
                    pressurepump: this.readUInt16(66),
                    water: this.readUInt16(68),
                    pressuretestbottle: this.readUInt16(70),
                    pressurecleanbottle: this.readUInt16(72),
                    isAlarm: this.readUInt16(74)
                }
                json.data.gps = {
                    timestamp: this.readUInt32(14) + this.options.base_time,
                    latitude: lonlat[1],
                    longitude: lonlat[0],
                    speed: this.readFloat(34),
                    azimuth: this.readFloat(38),
                    valid: this.readUInt16(42)
                }
                json.data.las = {
                    concentration: this.readFloat(48),
                    activeState: this.readUInt16(52),
                    counterM: this.readUInt16(54),
                    errorID: this.readUInt16(56)
                }

                if(lonlat[2]==false)
                    json.valid = false;

                json.event_code = this.status2event(json.data.sys.status)
                if(this.isError(_LAST_STATUS)){
                    if(!this.isError(json.data.sys.status))
                        json.event_code = EventCode.ERROR_END
                }
                _LAST_STATUS = json.data.sys.status

            }
        }
        return json
    }
}

module.exports = RealTimeResponse