const RealTimeResponse = require('./RealTimeResponse')
const SettingsResponse = require('./SettingsResponse')
const UnknownResponse = require('./UnknownResponse')
const TestGetResultResponse = require('./TestGetResultResponse')


class ResponseFactory {

    static create(options) {
        let response = null
        let message = options.message
        options.type = options.type || "unknown"
        //console.log("ResponseFactory:")
        //console.log( JSON.stringify(options.message) )
        let functionId = -1
        let dataLength = 0
        if(message.length > 2){
            functionId = message.readUIntBE(1, 1);
            dataLength = message.readUIntBE(2, 1);  // Legge il byte N.3
        }
        switch(functionId){
            case 3:
                console.log("creating <TestGetResultResponse>")
                response = new TestGetResultResponse(options)
                break
            case 4:
                if(dataLength != 40){
                    console.log("creating <RealTimeResponse>")
                    response = new RealTimeResponse(options)
                }else{
                    console.log("creating <SettingsResponse>")
                    response = new SettingsResponse(options)
                }
                break
            default:
                console.log("creating <UnknownResponse>")
                response = new UnknownResponse(options)
                break
        }
        return response
    }

}

module.exports = ResponseFactory