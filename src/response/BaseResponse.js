
class BaseResponse {
    /**
     * Constrcutor of the class
     * @param opts
     */
    constructor(options) {
        this.options = options
        this.options.type = options.type || 'unknown'
        if(!options)
            throw new Error("Missing options object")
    }

    parse() {
        throw new Error("Method not implemented")
    }
}

module.exports = BaseResponse