const number_util = require('number-util')
const RealTimeResponse = require('./RealTimeResponse')

class TestGetResultResponse extends RealTimeResponse{

    constructor(opts) {
        super(opts)
        this.options.type = "testgetresult"
    }

    /**
     #EEP_TEST_EXT_TYPE_ID            EEP_TEST_INT_RESPONSE_VALUE +4    // 0 - 2 byte  	uint16_t    typeId;
     #EEP_TEST_EXT_DURATION           EEP_TEST_EXT_TYPE_ID + 2          // 2 - 2 byte  	uint16_t    duration of test in ms;
     #EEP_TEST_EXT_OUTCOME			  EEP_TEST_EXT_DURATION + 2         // 4 - 2 byte  	uint16_t    outcome 0 ok  1 failed;
     #EEP_TEST_EXT_BOTTLE			  EEP_TEST_EXT_OUTCOME + 2			// 6 - 4 byte  	float       value of bottle;
     #EEP_TEST_EXT_RESPONSE_TIME      EEP_TEST_EXT_BOTTLE + 4           // 10 - 4 byte	float       response time;
     #EEP_TEST_EXT_TIMESTAMP          EEP_TEST_EXT_RESPONSE_TIME + 4    // 14 - 4 byte  uint32_t	timestamp test;
     #EEP_TEST_EXT_TIMESTAMPUTC       EEP_TEST_EXT_TIMESTAMP + 4        // 18 - 4 byte 	uint32_t    timestamp utc test;
     #EEP_TEST_EXT_OPENEV			  EEP_TEST_EXT_TIMESTAMPUTC + 4     // 22 - 2 byte  uint16_t    time to be open of the electrowalve in ms;
     #EEP_TEST_EXT_RESPONSE_VALUE     EEP_TEST_EXT_OPENEV + 2   	    // 24 - 4 byte 	float       response value;
     * @returns {{valid: boolean, data: {}}}
     */
    parse() {
        //[ID][FC][BC][DATA(1+)][CRC]
        let message = this.options.message || null
        let json = {
            valid: false,
            data: {}
        }
        if(message.length >= 5){
            let functionId = message.readUIntBE(1, 1);  // Legge il byte N.2
            let dataLength = message.readUIntBE(2, 1);  // Legge il byte N.3
            if (functionId == 3 && (message.length === 3 + dataLength + 2) && this.checkCRC(message)){
                json.type = "testgetresult"
                json.valid = true
                json.data = {
                    type: this.readUInt16(0),
                    duration: this.readUInt16(2),
                    outcome: this.readUInt16(4),
                    bottle: this.readFloat(6),
                    response_time: this.readFloat(10),
                    timestamp: this.readUInt32(14),
                    timestamp_utc: this.readUInt32(18),
                    openev: this.readUInt16(22),
                    ppm: this.readFloat(24)
                }
            }
        }
        return json
    }
}

module.exports = TestGetResultResponse