const net = require('net');
const MessageBuffer = require('./MessageBuffer')


class TcpIpClient {
    /**
     * Constructor
     * @param id - identifier of the connection
     * @param wsconn - websocket connection object
     */
    constructor(id, wsconn, timeout){
        this.id = id;
        this.client = new net.Socket()
        this.wsconn = wsconn
        this.timeout = timeout
        this.timeout_id = null
        this.handlers = null
        this.port = null
        this.host = null
        this.received = null
        this.processing = false
    }

    /**
     * Connect to the TCP/IP server
     * @param host - host name of IP address of the TCP/IP server
     * @param port - port number of the TCP/IP server
     * @param handlers - object handling events [onConnect, onData, conClose] in the TCP/IP communication
     */
    connect(host, port, handlers){
        let _this = this
        this.port = port
        this.host = host
        this.handlers = handlers
        handlers.onConnect = handlers.onConnect || function(){}
        handlers.onData = handlers.onData || function(){}
        handlers.onClose = handlers.onClose || function(){}
        this.client.connect({ port, host }, handlers.onConnect)
        this.received = new MessageBuffer()
        this.client.on("data", data => {
            //console.log("receing data..." + data)
            _this.received.push(data)
            if(_this.received.isFinished()){
                clearInterval(_this.timeout_id)
                let data = _this.received.handleData()
                console.log(data)
                handlers.onData(data)
                _this.processing = false
                /*
                _this.client.removeAllListeners()
                _this.client.destroy()
                _this.client = null
                _this.client = new net.Socket()
                _this.connect(host, port, handlers)
                */
            }
        })
        this.client.on('close', function(){
            console.log("Client is closed")
            handlers.onClose();
        })
        this.client.on('error', (e)=>{
            console.warn(e);
        })
    }


    buffToHexString(chunk){
        let arrByte = Uint8Array.from(chunk)
        let hexString = "";
        for(let i=0; i<arrByte.length; i++){
            let val = "" + arrByte[i].toString(16);
            hexString += "0x"
            if(val.length==1)
                hexString += "0"
            hexString += arrByte[i].toString(16) + ",";
        }
        if(hexString.length>0)
            hexString = hexString.substring(0, hexString.length-1)
        return hexString;
    }

    onTimeout(){
        console.log("Timeout");
        if(this.handlers){
            this.handlers.onData(null)
            //this.client.destroy()
            //this.client = new net.Socket()
            //this.client.connect({
            //    port:this.port,
            //    host: this.host
            //}, this.handlers.onConnect)
        }
        this.processing = false
    }

    send(message){
        console.error("Processing is: " + this.processing)
        if(!this.processing){
            this.processing = true
            this.timeout_id = setTimeout(this.onTimeout.bind(this), this.timeout)
            this.received = new MessageBuffer()
            this.client.write(message)
            //setTimeout(() => this.client.destroy(), 0);
        }else{
            if(this.handlers) {
                this.handlers.onData(null)
            }
        }
    }

    /**
     * Disconnect from the TCP/IP server
     */
    disconnect(){
        this.client.destroy();
    }

}

module.exports = TcpIpClient



