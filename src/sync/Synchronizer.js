
class Synchronizer {

    static create(options){
        let sync = null
        switch(options.type.toUpperCase()){
            case "RESTAPI":
                sync = require("./RestApiSynchronizer")
                break
            default:
                sync = require("./NullSynchronizer")
                break
        }
        return new sync(options)
    }

}

module.exports = Synchronizer