const SynchronizerInterface = require('./SynchronizerInterface')

class NullSynchronizer extends SynchronizerInterface{

    constructor(config){
        super(config)
    }

    execute(rows, token){
        return new Promise((resolve, reject) => {
            let count = rows.length || 0
            console.log("NullSynchronizer.execute(): on " + count + " items")
            resolve({data: true})
        })
    }

}

module.exports = NullSynchronizer