const axios = require('axios')
const SynchronizerInterface = require('./SynchronizerInterface')

class RestApiSynchronizer extends SynchronizerInterface{

    constructor(config){
        super(config)
    }

    execute(rows, token){
        let headers = {}
        if(null!=token)
            headers['Authorization'] = `Bearer ${token}`
        return axios.post(this.config.options.url, rows, { headers, timeout: 500 })
        /*
        .then((res) => {
            console.log(`statusCode: ${res.statusCode}`)
            callback(true, res)
        })
        .catch((error) => {
            console.error(error)
            callback(false, error)
        })
        */
    }

}

module.exports = RestApiSynchronizer