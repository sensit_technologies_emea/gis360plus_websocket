class SynchronizerInterface {
    constructor(config) {
        this.config = config
        if(!this.execute)
            throw new Error("Synchronizer must have execute method!");
    }
}
module.exports = SynchronizerInterface