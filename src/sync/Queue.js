const async = require('async');
const Synchronizer = require('./Synchronizer');
const Package = require('./Package');

class Queue {

    constructor(options){
        options = options || {}
        options.type = options.type || 'unknown'
        options.options = options.options || {}
        this.options = options
        this._concurrency = options.concurrency || 1
        this._wait_time = options.wait_time || 3000
        if(this._concurrency < 1) this._concurrency = 1
        let onEmptyQueue = options.onEmptyQueue || function() {
            console.log('all items have been processed');
        }
        this.packages = {}
        this.ids = []
        this.in_progress = 0
        this._queue = async.queue(this.work.bind(this), this._concurrency);
        this._queue.drain(onEmptyQueue);
        this._synchronizer = Synchronizer.create(options)
        this._token = null
    }

    setToken(token){
        this._token = token
        return this._token
    }

    callback(cbk){
        return function(){
            console.log("callback called...")
            cbk()
        }
    }

    work(item, callback){
        let object_callback = this.callback(callback)
        let _this = this
        let _id = item.id
        this.in_progress++
        console.log("In queue: " + this.ids.length + ", In progress: " + this.in_progress + ", current is: " + _id)
        this._synchronizer.execute(item.items, this._token)
            .then((result) => {
                _this.in_progress--
                _this.remove(_id)
                object_callback()
            }, (error)=>{
                console.log(error)
                //It retries putting at the end of the queue and sleeping some time
                _this.in_progress--
                _this._queue.push(item)
                //object_callback()
                setTimeout(function(){
                    object_callback()
                }, _this._wait_time)
            })
    }

    put(pck){
        let pack = null
        let count = this.ids.length
        if(Array.isArray(pck)){
            pack = new Package({
                items: pck
            })
        }else{
            pack = pck
        }
        let id = pack.id
        this.ids.push(id)
        this.packages[id] = pack
        this._queue.push(pack)
        return (this.ids.length == count+1)
    }

    remove(id){
        let count = this.ids.length
        delete this.packages[id]
        let index = this.ids.indexOf(id)
        if(index > -1)
            this.ids.splice(index, 1);
        return (this.ids.length == count-1)
    }

}

module.exports = Queue