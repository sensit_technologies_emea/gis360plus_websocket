let config = require('../../../config')

if(process.env.NODE_ENV == 'test')
    config = require('../../../test/config')

const default_tr = require('./Default')
const null_tr = require('./Null')

if(config.synchronizer.transformer.toLowerCase() == "default"){
    module.exports = default_tr
}else{
    module.exports = null_tr
}