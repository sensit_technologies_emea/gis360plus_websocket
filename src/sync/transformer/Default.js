module.exports = function(obj){

    /*
    {
        "valid":true,
        "event_code":0,
        "sessionid": 0,
        "data":{
            "sys":{
                "serial":1002,
                "timestamp":1901903455,
                "pumpstatus":1,
                "batterylevel":100,
                "status":30,
                "userid":0,
                "concentration":0,
                "isAbsolute":0,
                "water":217,
                "pressurepump":3956,
                "pressuretestbottle":1054,
                "pressurecleanbottle":531
            },
            "gps":{
                "timestamp":46336,
                "latitude":0,
                "longitude":0,
                "speed":0,
                "azimuth":0,
                "valid":0
            },
            "las":{
                "concentration":2.9000000953674316,
                "activeState":41,
                "counterM":47053,
                "errorID":0
             }
        }

     }
     */
    if(!obj.valid) return null
    if(!obj.data) return null
    if(!obj.data.sys) return null
    if(!obj.data.gps) return null
    if(!obj.data.las) return null
    let obj_tr = {
        "workyear": 2020,
        "eventcodeid": obj.event_code ? obj.event_code : 0,
        "userid": 1,
        "deviceid": 0,
        "instrumentid": 0,
        "systemtimestamp": obj.data.sys.timestamp,
        "gpstimestamp": obj.data.gps.timestamp,
        "savedtimestamp": 0,
        "status": obj.data.sys.status,
        "lat": parseFloat(obj.data.gps.latitude.toFixed(8)),
        "lon": parseFloat(obj.data.gps.longitude.toFixed(8)),
        "measure": parseFloat(obj.data.las.concentration.toFixed(10)),
        "secondarymeasure": obj.data.sys.concentration,
        "isrelative": (obj.data.sys.isAbsolute == 1 ? 0 : 1),
        "instrumentalarm": 0,
        "alarmnotification": 0,
        "batterylevel": obj.data.sys.batterylevel,
        "pumpflow":  obj.data.sys.pressurepump,
        "sessionid": obj.sessionId
    }
    return obj_tr
}