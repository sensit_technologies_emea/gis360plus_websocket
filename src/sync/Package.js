const Utils = require('../Utils')
const transfomer = require('./transformer/Transformer')

class Package {

    constructor(options){
        options = options || {}
        this.items = options.items || []
        this.id = Utils.getUniqueID()
        this.max_size = options.max_size || 10
        if(this.items.length > this.max_size)
            this.max_size = this.items.length
    }

    /**
     * Clean the package of its items
     */
    empty(){
        this.items=[]
        return (this.items.length == 0)
    }

    /**
     * Add un record to the package
     * @param item
     */
    add(item){
        let prev = this.items.length
        let item_tr = transfomer(item)
        if(item_tr)
            this.items.push(item_tr)
        else
            console.log("### item is null: " + JSON.stringify(item))
        return (this.items.length == prev + 1)
    }

    /**
     * Get the size of the package (number of items)
     * @returns {number}
     */
    size(){
        return this.items.length
    }

    /**
     * Return if the package can contains other elements
     * @returns {boolean}
     */
    hasSpace(){
        return this.max_size > this.size()
    }

}

module.exports = Package
