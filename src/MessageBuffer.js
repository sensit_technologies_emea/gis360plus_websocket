class MessageBuffer {

    constructor(delimiter) {
        delimiter = delimiter || [0xd, 0xa]
        if(typeof delimiter === "string" || delimiter instanceof String)
            delimiter = Buffer.from(delimiter, 'utf8');
        if(!Array.isArray(delimiter))
            delimiter = [delimiter]
        this.delimiter = delimiter
        this.buffer = null
        this.messageLength = null
    }

    isFinished() {
        for(let i=0; i < this.delimiter.length; i++){
            let lastByte = (this.buffer.length >= this.delimiter.length)
                ? this.buffer[this.buffer.length -i -1]
                : (this.buffer.length>0 ? '' : null)
            if (
                lastByte == null ||
                lastByte === this.delimiter[this.delimiter.length -i -1] ||
                this.buffer.length == this.messageLength
            ) {
                continue
            }else
                return false
        }
        return true
    }

    push(data) {
        if(typeof data === "string" || data instanceof String)
            data = Buffer.from(data, 'utf8');
        if(null==this.buffer )
            this.buffer = data
        else
            this.buffer = Buffer.concat([this.buffer, data])
        if(this.messageLength == null && this.buffer.length >= 3){
            let action = this.buffer.readUIntBE(1, 1)
            let dataLength = 0
            switch(action){
                case 3:
                case 4:
                    dataLength = this.buffer.readUIntBE(2, 1);
                    break
                case 5:
                    dataLength = 3
                    break
                case 129:
                case 130:
                case 131:
                case 132:
                case 133:
                case 134:
                case 143:
                case 144:
                    dataLength = 3
                    break
                default:
                    dataLength = 1000
                    break
            }
            //[ID][FC][BC][DATA(1+)][CRC(2)]
            this.messageLength = 3 + dataLength + 2
        }
    }

    getMessage() {
        let cont = 0
        for(let i=0; i< this.delimiter.length; i++){
            let lastByte = this.delimiter[this.delimiter.length -i -1]
            if(this.buffer[this.buffer.length -i -1] === lastByte)
                cont++
            else
                break
        }
        const message = this.buffer.slice(0, this.buffer.length-cont)
        //this.buffer = this.buffer.replace(message + this.delimiter, "")
        return message
    }

    handleData() {
        /**
         * Try to accumulate the buffer with messages
         *
         * If the server isnt sending delimiters for some reason
         * then nothing will ever come back for these requests
         */
        const message = this.getMessage()
        return message
    }
}

module.exports = MessageBuffer