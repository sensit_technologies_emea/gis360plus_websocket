process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const {TestGetResultResponse, ResponseFactory} = require('../../src/response/Response')


describe('TestGetResultResponse Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a TestGetResultResponse', (done) => {
        let response = ResponseFactory.create({type: "testgetresult", message: Buffer.from([0x33,0x3,0x3c])})
        assert.strictEqual((response instanceof TestGetResultResponse), true)
        done()
    })

    it('it should parse TestGetResultResponse INTERNAL response', (done) => {
        let response = ResponseFactory.create({
            type: "testgetresult",
            message: Buffer.from([0x33,0x3,0x3c,0x0,0x1,0x14,0x50,0x0,0x0,0xcc,0xcd,0x42,0x52,0x80,0x0,0x45,0xa2,0xc7,0xe2,0x7,0xcc,0xc7,0xe2,0x7,0xcc,0x0,0x32,0x0,0x0,0x40,0x40,0x0,0x2,0x13,0x88,0x0,0x0,0xcc,0xcd,0x42,0x52,0x60,0x0,0x45,0x23,0xc8,0x17,0x7,0xcc,0xc8,0x17,0x7,0xcc,0x7,0xd0,0x0,0x0,0x40,0x40,0x40,0x26,0x0,0x0,0xf0,0x3b])
        })
        let jsonResponse = response.parse()
        assert.strictEqual(jsonResponse.valid, true)
        assert.strictEqual(jsonResponse.data.type, 1)
        assert.strictEqual(jsonResponse.data.duration, 5200)
        assert.strictEqual(jsonResponse.data.outcome, 0)
        assert.strictEqual(jsonResponse.data.bottle.toFixed(1), "52.7")
        assert.strictEqual(jsonResponse.data.response_time, 5200)
        assert.strictEqual(jsonResponse.data.timestamp, 130861026)
        assert.strictEqual(jsonResponse.data.timestamp_utc, 130861026)
        assert.strictEqual(jsonResponse.data.openev, 50)
        assert.strictEqual(jsonResponse.data.ppm, 3)
        done()
    })

    it('it should parse TestGetResultResponse EXTERNAL response', (done) => {
        let response = ResponseFactory.create({
            type: "testgetresult",
            message: Buffer.from([0x33,0x3,0x3c,0x0,0x2,0x13,0x88,0x0,0x0,0xcc,0xcd,0x42,0x52,0x60,0x0,0x45,0x23,0xc8,0x17,0x7,0xcc,0xc8,0x17,0x7,0xcc,0x7,0xd0,0x0,0x0,0x40,0x40,0x40,0x26,0x0,0x0,0x0,0x0,0xcc,0xfe,0x41,0xe9,0x8a,0xde,0x0,0x0,0x64,0x1d,0xa,0x62,0x47,0xfd,0xa,0x62,0x0,0x0,0x99,0x9a,0x42,0x43,0x0,0x1b,0x0,0x0,0x92,0x97])
        })
        let jsonResponse = response.parse()
        assert.strictEqual(jsonResponse.valid, true)
        assert.strictEqual(jsonResponse.data.type, 2)
        assert.strictEqual(jsonResponse.data.duration, 5000)
        assert.strictEqual(jsonResponse.data.outcome, 0)
        assert.strictEqual(jsonResponse.data.bottle.toFixed(1), "52.7")
        assert.strictEqual(jsonResponse.data.response_time, 2614)
        assert.strictEqual(jsonResponse.data.timestamp, 130861079)
        assert.strictEqual(jsonResponse.data.timestamp_utc, 130861079)
        assert.strictEqual(jsonResponse.data.openev, 2000)
        assert.strictEqual(jsonResponse.data.ppm, 3)
        done()
    })

})