process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

let config = require('./config');
let testHelper = require('./TestHelper')
let tcpipClient = require('../src/TcpIpClient');
var MockServer = require("./MockTcpIpServer")
var mock = null

describe('TcpIpClient', () => {

    beforeEach((done) => {
        mock = new MockServer(config.tcpip.port)
        done();
    });

    afterEach((done) => {
        mock.close(done)
    });

    it('it should construct an instance', () => {
        let client = new tcpipClient()
        assert.strictEqual((client instanceof tcpipClient), true)
    })

    it('it should connect to a tcpip server', (done) => {
        let doneOnly = testHelper.once(done)
        let bufferRequest = Buffer.from([0x31, 0x04, 0xa7, 0xf8, 0x00, 0x1e, 0xd7, 0x77])
        let bufferResponse = Buffer.from([0x31,0x4,0x3c,0x0,0x1,0x0,0x0,0x17,0xe,0x7,0xcd,0x0,0x5,0x0,0x64,0x0,0x78,0x17,0xe,0x7,0xcd,0xe6,0x47,0x3f,0x91,0x78,0x18,0x40,0x26,0x47,0xae,0x7a,0xe1,0x2e,0x14,0x40,0x46,0x99,0x9a,0x3e,0x99,0x0,0x0,0x0,0x0,0x0,0x1,0x0,0x2,0x0,0x1,0x0,0x0,0xc0,0x20,0x0,0x29,0x58,0x67,0x0,0x0,0x0,0x0,0xa2,0x7f])
        mock.addResponse(bufferRequest, bufferResponse)
        let client = new tcpipClient()
        client.connect(config.tcpip.host, config.tcpip.port, {
            onConnect: function () {
                client.send(bufferRequest)
            },
            onData: function(data){
                let message = "data: " + data
                //assert.strictEqual(message.substring(0,15), "data: myreponse")
                client.disconnect()
                doneOnly()
            },
            onClose: function(){
                doneOnly()
            }
        })
    })

    it('test buffer to hex', () => {
        let client = new tcpipClient()
    })

    /*
    it('it should connect to a REAL tcpip server', (done) => {
        let doneOnly = testHelper.once(done)
        let client = new tcpipClient()
        let counter=0
        client.connect("46.18.29.13", 4001, {
            onConnect: function () {
                buffer = Buffer.from([0x31, 0x04, 0xa7, 0xf8, 0x00, 0x1e, 0xd7, 0x77]);
                client.send(buffer)
            },
            onData: function(data){
                counter+=1
                let message = client.buffToHexString(data)
                //console.log(counter + ": " + message)
                //assert.strictEqual(message, "data: myreponse")
                client.disconnect()
                doneOnly()
            },
            onClose: function(){
                doneOnly()
            }
        })
    })
    */

})