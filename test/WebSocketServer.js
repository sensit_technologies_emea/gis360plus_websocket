process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const WebSocket = require('ws');
let config = require('./config');
let WSServer = require('../src/WebSocketServer');
let MockServer = require('./MockTcpIpServer');
let mock = null
let ws_server = null
const testHelper = require('./TestHelper')

describe('WebSocketServer Suite', () => {

    beforeEach((done) => {
        mock = new MockServer(config.tcpip.port)
        done();
    });

    afterEach((done) => {
        mock.close(done)
    });

    it('it should create a WebSocket Server', (done) => {
        ws_server = new WSServer(config)
        assert.strictEqual((ws_server instanceof WSServer), true)
        done()
    })

    it('it should create a unique id (32 chars)', (done) => {
        let ws_server = new WSServer(config)
        let uq_id=ws_server.getUniqueID()
        assert.strictEqual(uq_id.length, 32)
        done()
    })

    it('it should connect to TCP/IP Server', (done) => {
        let cnt = 0
        let done_only = testHelper.once(done)
        let msg = JSON.stringify({"message": "test1"})
        mock.addResponse("test1", 'response1')
        ws_server = new WSServer(config, "fake")
        ws_server.start()
            .then(()=>{
                console.log("ws connection established from client and server")
            })
        const url = config.websocket.protocol + '://localhost:' + config.websocket.port
        const connection = new WebSocket(url)
        connection.onopen = () => {
            connection.send(JSON.stringify({
                type: "connect",
                message: {
                    host: "localhost",
                    port: parseInt(config.tcpip.port)
                }
            }));
        }
        connection.onclose = () => {
            done_only()
        }
        connection.onmessage = (e) => {
            cnt += 1
            if(cnt==1){
                assert.strictEqual(e.data.substring(0,5), "HELLO")
                connection.send(msg)
            } else if(cnt==2) {
                assert.strictEqual(e.data.substring(0,9), "response1")
                connection.close(1000, "stop")
                done_only()
            }
        }
    })

})