process.env.NODE_ENV = 'test';

let nock = require('nock')
let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const NullSynchronizer = require('../../src/sync/NullSynchronizer')

describe('NullSynchronizer Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should get a true answer from NullSynchronizer', (done)=>{
        let pck = new NullSynchronizer({})
        let rows = []
        pck.execute(rows, null)
        .then((result) => {
            assert.strictEqual(JSON.stringify(result), JSON.stringify({data: true}))
            done()
        })
    })

})