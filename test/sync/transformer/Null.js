process.env.NODE_ENV = 'test';

let nock = require('nock')
let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const NullTransformer = require('../../../src/sync/transformer/Null')

describe('NullTransformer Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should get the original object', ()=>{
        let obj = {
            "valid": "aaa",
            "property_2": 123
        }
        assert.strictEqual(obj, NullTransformer(obj))
    })

})