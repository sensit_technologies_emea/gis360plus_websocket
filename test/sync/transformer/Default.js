process.env.NODE_ENV = 'test';

let nock = require('nock')
let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const DefaultTransformer = require('../../../src/sync/transformer/Default')

describe('DefaultTransformer Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should get a null object if valid is false', ()=>{
        let obj = {
            valid: false
        }
        assert.strictEqual(null, DefaultTransformer(obj))
    })

    it('it should get a null object if data is undefined', ()=>{
        let obj = {
            valid: true
        }
        assert.strictEqual(null, DefaultTransformer(obj))
    })

    it('it should get a null object if data.sys is undefined', ()=>{
        let obj = {
            valid: true,
            data: {
                gps: {},
                las: {}
            }
        }
        assert.strictEqual(null, DefaultTransformer(obj))
    })

    it('it should get a null object if data.gps is undefined', ()=>{
        let obj = {
            valid: true,
            data: {
                sys: {},
                las: {}
            }
        }
        assert.strictEqual(null, DefaultTransformer(obj))
    })

    it('it should get a null object if data.las is undefined', ()=>{
        let obj = {
            valid: true,
            data: {
                sys: {},
                gps: {}
            }
        }
        assert.strictEqual(null, DefaultTransformer(obj))
    })

    it('it should set a default (0) code if event_code is undefined', ()=>{
        let obj = {
            "valid":true,
            "data":{
                "sys":{
                    "serial":1002,
                    "timestamp":1901903455,
                    "pumpstatus":1,
                    "batterylevel":100,
                    "status":30,
                    "userid":0,
                    "concentration":0,
                    "isAbsolute":0,
                    "water":217,
                    "pressurepump":3956,
                    "pressuretestbottle":1054,
                    "pressurecleanbottle":531
                },
                "gps":{
                    "timestamp":46336,
                    "latitude":0.00000000,
                    "longitude":0.00000000,
                    "speed":0,
                    "azimuth":0,
                    "valid":0
                },
                "las":{
                    "concentration":2.9000000953674316,
                    "activeState":41,
                    "counterM":47053,
                    "errorID":0
                }
            }
        }
        let obj_tr = DefaultTransformer(obj)
        assert.strictEqual(0, obj_tr.eventcodeid)
    })

    it('it should set a default (0) code if event_code is undefined', ()=>{
        let obj = {
            "valid":true,
            "event_code": 1,
            "data":{
                "sys":{
                    "serial":1002,
                    "timestamp":1901903455,
                    "pumpstatus":1,
                    "batterylevel":100,
                    "status":30,
                    "userid":0,
                    "concentration":0,
                    "isAbsolute":0,
                    "water":217,
                    "pressurepump":3956,
                    "pressuretestbottle":1054,
                    "pressurecleanbottle":531
                },
                "gps":{
                    "timestamp":1901903455,
                    "latitude":0.00000000,
                    "longitude":0.00000000,
                    "speed":0,
                    "azimuth":0,
                    "valid":0
                },
                "las":{
                    "concentration":2.9000000953674316,
                    "activeState":41,
                    "counterM":47053,
                    "errorID":0
                }
            }
        }
        let obj_tr = DefaultTransformer(obj)
        assert.strictEqual(obj.event_code, obj_tr.eventcodeid)
        assert.strictEqual(obj.data.sys.timestamp, obj_tr.systemtimestamp)
        assert.strictEqual(obj.data.gps.timestamp, obj_tr.gpstimestamp)
        assert.strictEqual(0, obj_tr.savedtimestamp)
        assert.strictEqual(obj.data.sys.status, obj_tr.status)
        assert.strictEqual(parseInt(obj.data.gps.latitude), parseInt(obj_tr.lat))
        assert.strictEqual(parseInt(obj.data.gps.longitude), parseInt(obj_tr.lon))
        assert.strictEqual(obj.data.las.concentration.toFixed(2), obj_tr.measure.toFixed(2))
        assert.strictEqual(obj.data.sys.concentration, obj_tr.secondarymeasure)
        assert.strictEqual(1-obj.data.sys.isAbsolute, obj_tr.isrelative)
    })

})