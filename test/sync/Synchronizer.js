process.env.NODE_ENV = 'test';

let nock = require('nock')
let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const Synchronizer = require('../../src/sync/Synchronizer')
const NullSynchronizer = require('../../src/sync/NullSynchronizer')
const RestApiSynchronizer = require('../../src/sync/RestApiSynchronizer')

describe('Synchronizer Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a NullSynchronizer instance', (done) => {
        let pck = Synchronizer.create({
            "type": "????",
            "options": { }
        })
        assert.strictEqual((pck instanceof NullSynchronizer), true)
        done()
    })

    it('it should create a RestApiSynchronizer instance', (done) => {
        let pck = Synchronizer.create({
            "type": "RestApi",
            "options": {
                "url": "http://localhost:xxxx/db/insert/rows"
            }
        })
        assert.strictEqual((pck instanceof RestApiSynchronizer), true)
        done()
    })

})