process.env.NODE_ENV = 'test';

let nock = require('nock')
let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const Queue = require('../../src/sync/Queue')

describe('Queue Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a Queue instance', (done) => {
        let queue = new Queue({})
        assert.strictEqual((queue instanceof Queue), true)
        done()
    })

    it('it should set a authentication token on the Queue instance', (done) => {
        let queue = new Queue({})
        let token = "mytoken"
        assert.strictEqual((queue.setToken(token)), token)
        done()
    })

    it('it should add a package element in the queue', (done) => {
        let id = "aabbcc"
        let queue = new Queue({})
        let pck = {
            id,
            items: []
        }
        assert.strictEqual(queue.put(pck), true)
        assert.strictEqual(queue.ids.length, 1)
        assert.strictEqual(queue.packages.hasOwnProperty(id), true)
        done()
    })

    it('it should remove a package element from the queue', (done) => {
        let id = "aabbcc"
        let queue = new Queue({})
        let pck = {
            id,
            items: []
        }
        queue.put(pck)
        assert.strictEqual(queue.remove(id), true)
        assert.strictEqual(queue.ids.length, 0)
        assert.strictEqual(queue.packages.hasOwnProperty(id), false)
        done()
    })

    it('it should process some packages from the queue', (done) => {
        let queue = new Queue({ onEmptyQueue: function(){
                done()
            }
        })
        let pck1 = {
            id: ()=>{
                return "aaaaaa"
            },
            items: [1,2,3]
        }
        let pck2 = {
            id: ()=>{
                return "bbbbbb"
            },
            items: [4,5]
        }
        let pck3 = {
            id: ()=>{
                return "cccccc"
            },
            items: [6]
        }
        queue.put(pck1)
        queue.put(pck2)
        queue.put(pck3)
    })

})

