process.env.NODE_ENV = 'test';

let nock = require('nock')
let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

const RestApiSynchronizer = require('../../src/sync/RestApiSynchronizer')

describe('RestApiSynchronizer Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should get a correct answer from RestApiSynchronizer without a token', (done)=>{
        let api_base_url = "http://www.fake.com"
        let api_path = "/db/insert/rows"
        let answer = {
            status: true
        }
        const scope = nock(api_base_url, {})
            .post(api_path)
            .reply(200, answer)

        let pck = new RestApiSynchronizer({
            "type": "RestApi",
            "options": {
                "url": api_base_url + api_path
            }
        })
        let rows = []
        pck.execute(rows, null)
        .then((result) => {
            assert.strictEqual(JSON.stringify(result.data), JSON.stringify(answer))
            done()
        })

    })

    it('it should get a correct answer from RestApiSynchronizer using a token', (done)=>{
        let api_base_url = "http://www.fake.com"
        let api_path = "/db/insert/rows"
        let token = "XXXYYYZZZWWW"
        let answer = {
            status: true
        }
        const scope = nock(api_base_url, {
            reqheaders: {
                authorization: 'Bearer ' + token,
            },
        })
        .post(api_path)
        .reply(200, answer)

        let pck = new RestApiSynchronizer({
            "type": "RestApi",
            "options": {
                "url": api_base_url + api_path
            }
        })
        let rows = []
        pck.execute(rows, token)
            .then((result) => {
                assert.strictEqual(JSON.stringify(result.data), JSON.stringify(answer))
                done()
            })
    })

})