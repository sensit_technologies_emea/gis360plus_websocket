process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const Package = require('../../src/sync/Package')


describe('Package Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a Package instance', (done) => {
        let pck = new Package()
        assert.strictEqual((pck instanceof Package), true)
        done()
    })

    it('it should return 0 size', (done) => {
        let pck = new Package()
        assert.strictEqual(pck.size(), 0)
        done()
    })

    it('it should add an item to the package', (done) => {
        let pck = new Package()
        let item = {value:0}
        assert.strictEqual(pck.add(item), true)
        done()
    })

    it('it should empty the package', (done) => {
        let pck = new Package()
        let item1 = {value: 1}
        let item2 = {value: 2}
        pck.add(item1)
        pck.add(item2)
        assert.strictEqual(pck.empty(), true)
        done()
    })

    it('it should answer that in the package there is space for other elements', (done) => {
        let pck = new Package()
        let item1 = {value: 1}
        pck.add(item1)
        assert.strictEqual(pck.hasSpace(), true)
        done()
    })

})