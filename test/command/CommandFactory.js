process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const {
    StartCommand, UnknownCommand, CleanLineCommand,
    ConnectCommand, PumpCommand, TestCommand, TestGetResultCommand,
    CommandFactory
} = require('../../src/command/Command')

const wrapCommand=function(commandObject){
    commandObject.private = {
        _server: {}
    }
    return commandObject
}

describe('CommandFactory Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a StartCommand', (done) => {
        let command = CommandFactory.create(
            wrapCommand({type: "start"})
        )
        assert.strictEqual((command instanceof StartCommand), true)
        done()
    })

    it('it should create an UnkonwnCommand', (done) => {
        let command = CommandFactory.create(
            wrapCommand({type: "????"})
        )
        assert.strictEqual((command instanceof UnknownCommand), true)
        done()
    })

    it('it should create an CleanLineCommand', (done) => {
        let command = CommandFactory.create(
            wrapCommand({
                type: "cleanline",
                message: { status: true }
            })
        )
        assert.strictEqual((command instanceof CleanLineCommand), true)
        done()
    })

    it('it should create an ConnectCommand', (done) => {
        let command = CommandFactory.create(
            wrapCommand({
                type: "connect"
            })
        )
        assert.strictEqual((command instanceof ConnectCommand), true)
        done()
    })

    it('it should create an PumpCommand', (done) => {
        let command = CommandFactory.create(
            wrapCommand({
                type: "pump",
                message: { status: true }
            })
        )
        assert.strictEqual((command instanceof PumpCommand), true)
        done()
    })

    it('it should create an TestCommand', (done) => {
        let command = CommandFactory.create(
            wrapCommand({
                type: "test",
                message: { type: "I" }
            })
        )
        assert.strictEqual((command instanceof TestCommand), true)
        done()
    })

    it('it should create an TestGetResultCommand', (done) => {
        let command = CommandFactory.create(
            wrapCommand({
                type: "testgetresult",
                message: { type: "I" }
            })
        )
        assert.strictEqual((command instanceof TestGetResultCommand), true)
        done()
    })

})