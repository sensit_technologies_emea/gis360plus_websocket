const { v4: uuidv4 } = require('uuid');

class TestHelper{

    static once(func){
        let uid = uuidv4()
        TestHelper.once_ids.push(uid)
        return function(){
            let index = TestHelper.once_ids.indexOf(uid)
            if(index != -1){
                TestHelper.once_ids.splice(index,1)
                func()
            }
        }
    }

    static sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}

TestHelper.once_ids=[];

module.exports = TestHelper