process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);

let config = require('./config');
let testHelper = require('./TestHelper')
let MessageBuffer = require('../src/MessageBuffer');


describe('MessageBuffer', () => {

    beforeEach((done) => {
         done();
    });

    afterEach((done) => {
        done()
    });

    it('it should construct an instance', () => {
        let msg = new MessageBuffer(0x13)
        assert.strictEqual((msg instanceof MessageBuffer), true)
    })

    it('it should add a buffer chunck', () => {
        let msg = new MessageBuffer(0x13)
        let buffer = Buffer.from([0x31, 0x03, 0x75, 0x80, 0x00, 0x02, 0xda, 0x1f])
        msg.push(buffer)
        assert.strictEqual(msg.buffer.length, buffer.length)
        let buffer2 = Buffer.from([0x00, 0x00])
        msg.push(buffer2)
        assert.strictEqual(msg.buffer.length, buffer.length + buffer2.length)
    })

    it('it should add a buffer as string', () => {
        let msg = new MessageBuffer(0x13)
        msg.push("buffer\n")
        assert.strictEqual(msg.buffer.length, 7)
        assert.strictEqual(msg.buffer instanceof Buffer, true)
    })

    it('it should check that the message is not finished', () => {
        let msg = new MessageBuffer(0x13)
        let buffer = Buffer.from([0x31, 0x03, 0x75, 0x80, 0x00, 0x02, 0xda, 0x1f])
        msg.push(buffer)
        assert.strictEqual(msg.isFinished(), false)
    })

    it('it should check that the message is finished', () => {
        let msg = new MessageBuffer(0x13)
        let buffer = Buffer.from([0x31, 0x03, 0x75, 0x80, 0x00, 0x02, 0xda, 0x13])
        msg.push(buffer)
        assert.strictEqual(msg.isFinished(), true)
    })

    it('it should check that the message is finished (modbus sequence)', () => {
        let msg = new MessageBuffer(0x13)
        let buffer = Buffer.from([0x31,0x4,0x3c,0x0,0x1,0x0,0x0,0x17,0xe,0x7,0xcd,0x0,0x5,0x0,0x64,0x0,0x78,0x17,0xe,0x7,0xcd,0xe6,0x47,0x3f,0x91,0x78,0x18,0x40,0x26,0x47,0xae,0x7a,0xe1,0x2e,0x14,0x40,0x46,0x99,0x9a,0x3e,0x99,0x0,0x0,0x0,0x0,0x0,0x1,0x0,0x2,0x0,0x1,0x0,0x0,0xc0,0x20,0x0,0x29,0x58,0x67,0x0,0x0,0x0,0x0,0xa2,0x7f])
        msg.push(buffer)
        assert.strictEqual(msg.isFinished(), true)
    })

    it('it should check that the message is finished (modbus sequence in 2 parts)', () => {
        let msg = new MessageBuffer(0x13)
        let buffer = Buffer.from([0x31,0x4])
        msg.push(buffer)
        assert.strictEqual(msg.isFinished(), false)
        let buffer2 = Buffer.from([0x3c,0x0,0x1,0x0,0x0,0x17,0xe,0x7,0xcd,0x0,0x5,0x0,0x64,0x0,0x78,0x17,0xe,0x7,0xcd,0xe6,0x47,0x3f,0x91,0x78,0x18,0x40,0x26,0x47,0xae,0x7a,0xe1,0x2e,0x14,0x40,0x46,0x99,0x9a,0x3e,0x99,0x0,0x0,0x0,0x0,0x0,0x1,0x0,0x2,0x0,0x1,0x0,0x0,0xc0,0x20,0x0,0x29,0x58,0x67,0x0,0x0,0x0,0x0,0xa2,0x7f])
        msg.push(buffer2)
        assert.strictEqual(msg.isFinished(), true)
    })

    it('it should check that the message is finished (modbus sequence in 3 parts)', () => {
        let msg = new MessageBuffer(0x13)
        let buffer = Buffer.from([0x31,0x4])
        msg.push(buffer)
        assert.strictEqual(msg.isFinished(), false)
        let buffer2 = Buffer.from([0x3c,0x0,0x1,0x0,0x0,0x17,0xe,0x7])
        msg.push(buffer2)
        assert.strictEqual(msg.isFinished(), false)
        let buffer3 = Buffer.from([0xcd,0x0,0x5,0x0,0x64,0x0,0x78,0x17,0xe,0x7,0xcd,0xe6,0x47,0x3f,0x91,0x78,0x18,0x40,0x26,0x47,0xae,0x7a,0xe1,0x2e,0x14,0x40,0x46,0x99,0x9a,0x3e,0x99,0x0,0x0,0x0,0x0,0x0,0x1,0x0,0x2,0x0,0x1,0x0,0x0,0xc0,0x20,0x0,0x29,0x58,0x67,0x0,0x0,0x0,0x0,0xa2,0x7f])
        msg.push(buffer3)
        assert.strictEqual(msg.isFinished(), true)
    })

    it('it should check that the message is finished (from string with terminator)', () => {
        let msg = new MessageBuffer(0xa)
        msg.push("buffer\n")
        assert.strictEqual(msg.isFinished(), true)
    })

    it('it should check that the message is not finished (from string without correct terminator \\n and constructor without parameters)', () => {
        let msg = new MessageBuffer()
        msg.push("buffer\n")
        assert.strictEqual(msg.isFinished(), false)
    })

    it('it should check that the message is not finished (from string without correct terminator \\r and constructor without parameters)', () => {
        let msg = new MessageBuffer()
        msg.push("buffer\r")
        assert.strictEqual(msg.isFinished(), false)
    })

    it('it should check that the message is finished (from string with correct terminator and constructor without parameters)', () => {
        let msg = new MessageBuffer()
        msg.push("buffer\r\n")
        assert.strictEqual(msg.isFinished(), true)
    })

})