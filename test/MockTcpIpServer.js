const net = require('net');
const terminator = require('http-terminator')
const testHelper = require('./TestHelper')
const MessageBuffer = require('../src/MessageBuffer')

class MockTcpIpServer {
    constructor(port){
        let _this = this
        this.responses={}
        this.clients=[]
        this.server = net.createServer(function(socket) {
            _this.clients.push(socket)
            socket.on('data', function(message){
                let hexMessage = _this.toHex(message)
                if(_this.responses[hexMessage]){
                    socket.write(_this.responses[hexMessage] + '\r\n');
                    socket.pipe(socket);
                }
            });
            //socket.write('Connection established with Mock TcpIp: ' + JSON.stringify(_this.server.address()) + '\r\n');
            //socket.pipe(socket);
        });

        this.server.listen(port, function(){
            //console.log("I am ready to listen...")
        })
        this.httpTerminator = terminator.createHttpTerminator({server: this.server})
    }

    close(done){
        let _this = this
        for (let i=0; i<this.clients.length;i++) {
            this.clients[i].destroy();
        }
        this.clients=[]
        let address = this.server.address()
        if(null==address){
            done()
        }else{
            this.httpTerminator.terminate().then(()=>{
                _this.server.unref();
                testHelper.sleep(200).then(()=>{
                    done()
                })
            }, (error)=>{
                console.error("---MockTcpIpServer closed NOT correctly: " + error)
                _this.server.unref();
                testHelper.sleep(200).then(()=>{
                    done()
                })
            })
        }
    }

    toHex(message){
        let bufferMessage = Buffer.from(message)
        return "mock_" + bufferMessage.toString('hex')
    }

    addResponse(message, response){
        /*
        let received = new MessageBuffer()
        received.push(message)
        let data = received.handleData()
         */
        let key = this.toHex(message)
        this.responses[key] = response
        return this;
    }

}

module.exports = MockTcpIpServer