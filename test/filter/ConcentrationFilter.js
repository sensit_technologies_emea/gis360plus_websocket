process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const ConcentrationFilter = require('../../src/filter/ConcentrationFilter')
const EventCode = require('../../src/EventCode')

describe('ConcentrationFilter Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a ConcentrationFilter instance', (done) => {
        let filter = new ConcentrationFilter()
        assert.strictEqual((filter instanceof ConcentrationFilter), true)
        done()
    })

    it('it should check a value under the threshold limit', (done) => {
        let filter = new ConcentrationFilter({limit: 20})
        let json = {
            valid: true,
            data: {
                las: {
                    concentration: 10
                }
            }
        }
        assert.strictEqual(filter.check(json), true)
        done()
    })

    it('it should check a value over the threshold limit', (done) => {
        let filter = new ConcentrationFilter({limit: 20})
        let json = {
            valid: true,
            data: {
                las: {
                    concentration: 100
                }
            }
        }
        assert.strictEqual(filter.check(json), false)
        assert.strictEqual(json.event_code, EventCode.LEAK)
        done()
    })

    it('it should check multiple values', (done) => {
        let filter = new ConcentrationFilter({limit: 20})
        let json1 = {
            valid: true,
            event_code: 0,
            data: {
                las: {
                    concentration: 100
                }
            }
        }
        let json2 = {
            valid: true,
            event_code: 0,
            data: {
                las: {
                    concentration: 90
                }
            }
        }

        let json3 = {
            valid: true,
            event_code: 0,
            data: {
                las: {
                    concentration: 19
                }
            }
        }

        let json4 = {
            valid: true,
            event_code: 0,
            data: {
                las: {
                    concentration: 25
                }
            }
        }

        assert.strictEqual(filter.check(json1), false)
        assert.strictEqual(json1.event_code, EventCode.LEAK)

        assert.strictEqual(filter.check(json2), true)
        assert.strictEqual(json2.event_code, 0)

        assert.strictEqual(filter.check(json3), true)
        assert.strictEqual(json2.event_code, 0)

        assert.strictEqual(filter.check(json4), false)
        assert.strictEqual(json1.event_code, EventCode.LEAK)

        done()
    })

})