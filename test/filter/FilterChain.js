process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const FilterChain = require('../../src/filter/FilterChain')
const EventCode = require('../../src/EventCode')

describe('FilterChain Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a FilterChain instance', (done) => {
        let chain = new FilterChain()
        assert.strictEqual((chain instanceof FilterChain), true)
        done()
    })

    it('it should check a empty FilterChain', (done) => {
        let chain = new FilterChain()
        let json = {
            valid: true,
            data: {
                gps: {
                    speed: 100
                }
            }
        }
        assert.strictEqual(chain.check(json), true)
        done()
    })

    it('it should check a FilterChain with a single filter', (done) => {
        let chain = new FilterChain({
            name: "speed",
            options: {
                "limit": 30
            }
        })
        let json = {
            valid: true,
            event_code: 0,
            data: {
                gps: {
                    speed: 100
                }
            }
        }
        assert.strictEqual(chain.check(json), false)
        assert.strictEqual(json.event_code, EventCode.SPEED_EXCEEDED)
        done()
    })

    it('it should check a FilterChain with a 2 filters (speed+concentration)', (done) => {
        let chain = new FilterChain([
            { name: "speed", options: { limit: 30 } },
            { name: "concentration", options: { limit: 100 } }
        ])
        let json = {
            valid: true,
            event_code: 0,
            data: {
                gps: {
                    speed: 100
                },
                las: {
                    concentration: 250
                }
            }
        }
        assert.strictEqual(chain.check(json), false)
        assert.strictEqual(json.event_code, EventCode.SPEED_EXCEEDED)
        done()
    })

    it('it should check a FilterChain with a 2 filters (concentration+speed)', (done) => {
        let chain = new FilterChain([
            { name: "concentration", options: { limit: 100 } },
            { name: "speed", options: { limit: 30 } }
        ])
        let json = {
            valid: true,
            event_code: 0,
            data: {
                gps: {
                    speed: 100
                },
                las: {
                    concentration: 250
                }
            }
        }
        assert.strictEqual(chain.check(json), false)
        assert.strictEqual(json.event_code, EventCode.LEAK)
        done()
    })

    it('it should check a FilterChain with a 2 filters (concentration+speed) only speed exceeded', (done) => {
        let chain = new FilterChain([
            { name: "concentration", options: { limit: 100 } },
            { name: "speed", options: { limit: 30 } }
        ])
        let json = {
            valid: true,
            event_code: 0,
            data: {
                gps: {
                    speed: 100
                },
                las: {
                    concentration: 50
                }
            }
        }
        assert.strictEqual(chain.check(json), false)
        assert.strictEqual(json.event_code, EventCode.SPEED_EXCEEDED)
        done()
    })

})