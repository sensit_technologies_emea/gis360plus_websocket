process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const SpeedFilter = require('../../src/filter/SpeedFilter')
const EventCode = require('../../src/EventCode')

describe('SpeedFilter Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a SpeedFilter instance', (done) => {
        let filter = new SpeedFilter()
        assert.strictEqual((filter instanceof SpeedFilter), true)
        done()
    })

    it('it should check a value under the threshold limit', (done) => {
        let filter = new SpeedFilter({limit: 20})
        let json = {
            valid: true,
            data: {
                gps: {
                    speed: 10
                }
            }
        }
        assert.strictEqual(filter.check(json), true)
        done()
    })

    it('it should check a value over the threshold limit', (done) => {
        let filter = new SpeedFilter({limit: 20})
        let json = {
            valid: true,
            data: {
                gps: {
                    speed: 100
                }
            }
        }
        assert.strictEqual(filter.check(json), false)
        assert.strictEqual(json.event_code, EventCode.SPEED_EXCEEDED)
        done()
    })

})