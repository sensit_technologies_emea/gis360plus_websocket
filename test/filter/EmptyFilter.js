process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const EmptyFilter = require('../../src/filter/EmptyFilter')


describe('EmptyFilter Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a EmptyFilter instance', (done) => {
        let filter = new EmptyFilter()
        assert.strictEqual((filter instanceof EmptyFilter), true)
        done()
    })

    it('it should check a value without change original data', (done) => {
        let filter = new EmptyFilter()
        let data = {value: "fake"}
        assert.strictEqual(filter.check(data), true)
        assert.strictEqual(data.value, "fake")
        done()
    })
    
})