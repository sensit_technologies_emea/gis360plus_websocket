process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = require('assert');
let should = chai.should();
chai.use(chaiHttp);
const {EmptyFilter, SpeedFilter, ConcentrationFilter, FilterFactory} = require('../../src/filter/Filter')


describe('FilterFactory Suite', () => {

    beforeEach((done) => {
        done()
    });

    afterEach((done) => {
        done()
    });

    it('it should create a EmptyFilter instance', (done) => {
        let filter = FilterFactory.create({type:"unknown"})
        assert.strictEqual((filter instanceof EmptyFilter), true)
        done()
    })

    it('it should create a SpeedFilter instance', (done) => {
        let filter = FilterFactory.create({type:"speed"})
        assert.strictEqual((filter instanceof SpeedFilter), true)
        done()
    })

    it('it should create a ConcentrationFilter instance', (done) => {
        let filter = FilterFactory.create({type:"concentration"})
        assert.strictEqual((filter instanceof ConcentrationFilter), true)
        done()
    })

})