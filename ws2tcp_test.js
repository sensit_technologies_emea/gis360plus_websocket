// Include Nodejs' net module.
const Net = require('net');
// The port number and hostname of the server.
const port = 5001;
const host = '188.147.173.141';
const id = 0x31


buffToHexString=function(chunk){
	var arrByte = Uint8Array.from(chunk)
	hexString = "";
	for(var i=0; i<arrByte.length; i++){
		val = "" + arrByte[i].toString(16);
		hexString += "0x"
		if(val.length==1)
			hexString += "0"
		hexString += arrByte[i].toString(16) + ", ";
	}
	return hexString;
}

// Create a new TCP client.
const client = new Net.Socket();
// Send a connection request to the server.
client.connect({ port: port, host: host }, function() {
    // If there is no error, the server has accepted the request and created a new 
    // socket dedicated to us.
    console.log('TCP connection established with the server.');
    // The client can now send data to the server by writing to its socket.
	buffer = Buffer.from([id, 0x03, 0x75, 0x80, 0x00, 0x02, 0xda, 0x1f]);
	client.write(buffer);
	hexString = buffToHexString(buffer);
	console.log(`Data sent to the server:       ${hexString}.`);
});

// The client can also receive data from the server by reading from its socket.
client.on('data', function(chunk) {
	hexString = buffToHexString(chunk);
    console.log(`Data received from the server: ${hexString}.`);
    // Request an end to the connection after the data has been received.
    client.end();
});

client.on('end', function() {
    console.log('Requested an end to the TCP connection');
});